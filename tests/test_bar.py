import pytest
from pytest_mock import MockFixture
from unittest.mock import call, MagicMock
import asyncio

from lemonsqueezer import Align, Bar

from . import MockModule, MockButton, mock_coroutine


class TestUpdating:
    @pytest.mark.parametrize('alignment', list(Align))
    def test_register_module_adds_updater(self, alignment):
        module = MockModule(align=alignment)
        bar = Bar()
        updaters_before = len(bar.module_updaters)

        bar.register_module(module)

        assert len(bar.module_updaters) == updaters_before + 1

    @pytest.mark.asyncio
    @pytest.mark.parametrize('alignment', list(Align))
    async def test_first_updater_calls_setup(self, alignment, mocker: MockFixture):
        module = MockModule(align=alignment)
        bar = Bar()

        bar.register_module(module)
        await bar.module_updaters.pop()()

        assert module.setup.call_args == call(bar)

    @pytest.mark.asyncio
    @pytest.mark.parametrize('alignment', list(Align))
    async def test_second_updater_calls_get_output(self, alignment):
        module = MockModule(align=alignment)
        bar = Bar()

        bar.register_module(module)
        first_update = await bar.module_updaters.pop()()
        second_updater = first_update.new_updater
        await second_updater()

        assert module.get_output.call_count == 1

    @pytest.mark.asyncio
    @pytest.mark.parametrize('alignment', list(Align))
    async def test_setup_called_once(self, alignment):
        module = MockModule(align=alignment)
        bar = Bar()

        bar.register_module(module)
        updater = (await bar.module_updaters.pop()()).new_updater
        for _ in range(5):
            updater = (await updater()).new_updater

        assert module.setup.call_count == 1

    @pytest.mark.parametrize('alignment', list(Align))
    def test_register_module_uses_correct_module_list(self, alignment):
        bar = Bar()

        module = MockModule(align=alignment)

        module_list = bar.modules[alignment]
        modules_before = len(module_list)

        bar.register_module(module)

        assert len(module_list) == modules_before + 1

    @pytest.mark.parametrize('alignment', [None, 2, 'Left', 'LEFT', object()])
    def test_register_needs_valid_alignment(self, alignment):
        bar = Bar()
        module = MockModule(align=alignment)

        with pytest.raises(SystemExit):
            bar.register_module(module)

    # TODO Test actual output to bar
    @pytest.mark.asyncio
    async def test_writes_to_bar(self, mocker):
        bar = Bar()
        bar._bar = MagicMock(returncode=None)

        await bar._update_bar()

        assert bar._bar.stdin.write.call_count == 1

    @pytest.mark.asyncio
    async def test_write_to_dead_bar_stops_loop(self):
        loop = asyncio.get_event_loop()
        loop.stop = MagicMock(side_effect=loop.stop)
        bar = Bar(_loop=loop)
        bar._bar = await asyncio.create_subprocess_exec('echo', 'test', stdin=asyncio.subprocess.PIPE)
        await bar._bar.wait()

        await bar._write_to_bar('test')

        assert loop.stop.call_count == 1



    @pytest.mark.asyncio
    async def test_exception_module_does_not_crash_bar(self):
        bar = Bar()
        # To stop the updater loop
        def mock_bar_update():
            raise ValueError
        bar._update_bar = mock_bar_update
        
        async def mock_updater():
            raise Exception
        bar.module_updaters.append(mock_updater)

        try:
            await bar._updater()
        except ValueError:
            pass


class TestButtons:
    def test_register_button_adds_all_callbacks(self):
        bar = Bar()
        button = MockButton()
        button.callbacks = [('id1', 1, lambda: 'button1'), ('id2', 2, lambda: 'button2')]

        bar.register_button(button)

        assert bar.button_callbacks['id1']() == 'button1'
        assert bar.button_callbacks['id2']() == 'button2'

    def test_button_helper_passes_all_args(self, mocker: MockFixture):
        bar = Bar()
        mock_init: MagicMock = mocker.patch('lemonsqueezer.Button.__init__')
        mock_init.return_value = None
        bar.register_button = MagicMock()

        bar.button('these', 'are', 'some', keyword='args')

        assert mock_init.call_args == call('these', 'are', 'some', keyword='args')
