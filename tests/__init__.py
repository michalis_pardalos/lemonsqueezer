from unittest.mock import Mock, MagicMock
import asyncio
from lemonsqueezer import BarModule, Button

def mock_coroutine(return_value=None):
    """
    Create a coroutine for mocking. It just instantly returns the return value

    :param return_value: What the coroutine should return
    """
    coroutine = MagicMock()

    future = asyncio.Future()
    future.set_result(return_value)
    coroutine.return_value = future

    return coroutine

    
def MockModule(*args, **kwargs):
    module = Mock(spec=BarModule, *args, **kwargs)

    module.setup = mock_coroutine("setup")
    module.get_output = mock_coroutine("output")
    
    return module


def MockButton(*args, **kwargs):
    return Mock(spec=Button, *args, **kwargs)
