from lemonsqueezer import Button

sample_text = 'testytest'

def test_has_text():
    b = Button(sample_text, (1, lambda: None))
    assert b.text == sample_text

def test_single_callback():
    b = Button(sample_text, 1, lambda: 'correct')
    assert b.callbacks[0][1] == 1
    assert b.callbacks[0][2]() == 'correct'

def test_multiple_callbacks():
    b = Button(sample_text, (1, lambda: 'b1'), (2, lambda: 'b2'))

    assert b.callbacks[0][1] == 1
    assert b.callbacks[0][2]() == 'b1'

    assert b.callbacks[1][1] == 2
    assert b.callbacks[1][2]() == 'b2'

def test_enough_closing_buttons():
    b = Button(sample_text, (1, lambda: None), (2, lambda: None))
    assert str(b).count('%{A}') == 2

def test_dynamic_string():
    b = Button(sample_text, 1, lambda: None)
    b.text = 'other'
    assert sample_text not in str(b)
    assert 'other' in str(b)
