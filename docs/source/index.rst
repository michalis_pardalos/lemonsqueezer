.. include:: ../../README.md

.. toctree::
    :hidden:
    :caption: Contents 
    
    Home <self>
    API Reference <reference> 

    Package Index <genindex>
